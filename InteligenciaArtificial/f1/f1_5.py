#Instalar numpy:
#pip install --user numpy scipy matplotlib ipython jupyter pandas sympy nose
import math
import numpy as np

def lerMatriz():

	M = np.zeros([2,2])

	for i in range(0,2):
		for j in range(0,2):
			M[i,j] = input('M[' + str(i+1) + ',' + str(j+1) + ']: ')

	return M

def escreveMatriz(M):

	for i in range(0,2):
			print(str(M[i,0]) + '  ' + str(M[i,1]))
	print()

def produtoElemento(A,B):

	M = np.zeros([2,2])

	for i in range(0,2):
		for j in range(0,2):
			M[i,j]=A[i,j]*B[i,j]

	return M

def produtoMatricial(A,B):

	M = np.zeros([2,2])

	for i in range(0,2):
		for j in range(0,2):
			for k in range(0,2):
				M[i,j]+=A[i,k]*B[k,j]

	return M

def diferencaMatricial(A,B):

	M = np.zeros([2,2])

	for i in range(0,2):
		for j in range(0,2):
			M[i,j]=A[i,j]-B[i,j]

	return M

def logaritmoMatricial(A):

	M = np.zeros([2,2])

	for i in range(0,2):
		for j in range(0,2):
			M[i,j]=abs(math.log(A[i,j]))

	return M

def maior2lAmenor1cB(A,B):

	if A[1,0] > A[1,1]:
		maior2lA = A[1,0]
	else:
		maior2lA = A[1,1]

	if B[0,0] < B[1,0]:
		menor1cB = B[0,0]
	else:
		menor1cB = B[1,0]
	
	return maior2lA*menor1cB

print('Ler Matriz A:')
A = lerMatriz();
print('Ler Matriz B:')
B = lerMatriz();

print()
escreveMatriz(produtoElemento(A,B))
escreveMatriz(produtoMatricial(A,B))	
escreveMatriz(diferencaMatricial(A,B))
escreveMatriz(logaritmoMatricial(A))
print(maior2lAmenor1cB(A,B));