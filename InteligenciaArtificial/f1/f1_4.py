def lerListaInteiros(frase):
	
	print(frase)
	Lista = []
	conta=1

	while True:
		x = int(input(str(conta) + ': '))
		conta+=1
		if x < 0:
			break
		Lista.append(x)

	return Lista

def intersecao(listaA,listaB):

	Lista = []

	for a in listaA:
		if a in listaB and a not in Lista:
			Lista.append(a)

	return Lista

listaA = lerListaInteiros("Lista A:")
listaB = lerListaInteiros("Lista B:")

listaC = intersecao(listaA,listaB)

print("Repetidos:")
for a in listaC:
	print(str(a) + " ",end="")