def ler(frase):
	frase = input(frase)
	return frase

def escreve(nomeFicheiro,frase):
	ficheiro = open(nomeFicheiro,"w")
	ficheiro.write(frase)
	ficheiro.close

def contaVogais(frase):
	conta=0
	for a in str(frase):
		if a in {'a','e','i','o','u'}:
			conta+=1
	return conta

fraseA = ler("Escreve a frase A: ")
fraseB = ler("Escreve a frase B: ")

if contaVogais(fraseA) > contaVogais(fraseB):
	escreve('maisVogais.txt',fraseA)
else:
	escreve('maisvogais.txt',fraseB)